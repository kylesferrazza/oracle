{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "oracle";
  buildInputs = with pkgs; [
    terraform
    ansible
  ];
}
