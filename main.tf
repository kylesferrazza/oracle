terraform {
  backend "remote" {
    organization = "kylesferrazza"

    workspaces {
      name = "oracle"
    }
  }
}

variable "tenancy_ocid" {
  type = string
}

variable "compartment_ocid" {
  type = string
}

variable "user_ocid" {
  type = string
}

variable "fingerprint" {
  type = string
}

variable "private_key" {
  type = string
}

variable "region" {
  type = string
}

variable "availability_domain" {
  type = string
}

variable "ssh_pubkey" {
  type = string
}

variable "hostname" {
  type = string
  default = "ks-free"
}

provider "oci" {
  tenancy_ocid = var.tenancy_ocid
  user_ocid = var.user_ocid
  fingerprint = var.fingerprint
  private_key = var.private_key
  region = var.region
}

resource "oci_core_vcn" "vcn" {
  cidr_block = "10.1.0.0/16"
  compartment_id = var.compartment_ocid
  display_name = "vcn-${var.hostname}"
  dns_label = "vcn${var.hostname}"
}

resource "oci_core_internet_gateway" "internet_gateway" {
  compartment_id = var.compartment_ocid
  display_name = "ig-${var.hostname}"
  vcn_id = oci_core_vcn.vcn.id
}

resource "oci_core_default_route_table" "default_route_table" {
  manage_default_resource_id = oci_core_vcn.vcn.default_route_table_id
  display_name = "rt-${var.hostname}"

  route_rules {
    destination = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    network_entity_id = oci_core_internet_gateway.internet_gateway.id
  }
}

resource "oci_core_network_security_group_security_rule" "nsg_outbound" {
  network_security_group_id = oci_core_network_security_group.nsg.id
  direction = "EGRESS"
  protocol = "all"
  description = "nsg-${var.hostname}-outbound"
  destination = "0.0.0.0/0"
  destination_type = "CIDR_BLOCK"
}

resource "oci_core_subnet" "subnet" {
  availability_domain = var.availability_domain
  cidr_block = "10.1.0.0/24"
  display_name = "subnet-${var.hostname}"
  dns_label = "subnet${var.hostname}"
  security_list_ids = [
    oci_core_security_list.empty_security_list.id
  ]
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  route_table_id = oci_core_vcn.vcn.default_route_table_id
  dhcp_options_id = oci_core_vcn.vcn.default_dhcp_options_id
}

resource "oci_core_security_list" "empty_security_list" {
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  display_name = "seclist-${var.hostname}"
}

resource "oci_core_network_security_group" "nsg" {
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  display_name = "nsg-${var.hostname}"
}

resource "oci_core_network_security_group_security_rule" "nsg_inbound_ssh" {
  count = var.num_instances
  network_security_group_id = oci_core_network_security_group.nsg.id
  direction = "INGRESS"
  protocol = "6" # TCP
  description = "nsg-${var.hostname}-inbound-ssh"
  source = "0.0.0.0/0"
  source_type = "CIDR_BLOCK"
  destination = "${oci_core_instance.instance[count.index].public_ip}/32"
  destination_type = "CIDR_BLOCK"
  tcp_options {
    destination_port_range {
      min = 22
      max = 22
    }
  }
}

data "oci_core_images" "ubuntu_images" {
  compartment_id = var.compartment_ocid
  operating_system = "Canonical Ubuntu"
  operating_system_version = "20.04"
  sort_by = "TIMECREATED"
}

variable "num_instances" {
  type = number
  default = 2
}

resource "oci_core_instance" "instance" {
  count = var.num_instances
  availability_domain = var.availability_domain
  display_name = "${var.hostname}-${count.index}"
  compartment_id = var.compartment_ocid
  shape = "VM.Standard.E2.1.Micro"
  create_vnic_details {
    hostname_label = "${var.hostname}-${count.index}"
    subnet_id = oci_core_subnet.subnet.id
    nsg_ids = [oci_core_network_security_group.nsg.id]
  }
  source_details {
    source_id = data.oci_core_images.ubuntu_images.images[0].id
    source_type = "image"
  }
  metadata = {
    ssh_authorized_keys = var.ssh_pubkey
  }
}

output "instance_public_ips" {
  value = [for instance in oci_core_instance.instance: instance.public_ip]
}

variable "cloudflare_api_token" {
  type = string
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

variable "cloudflare_zone_id" {
  type = string
}

resource "cloudflare_record" "record" {
  count = var.num_instances
  zone_id = var.cloudflare_zone_id
  name = oci_core_instance.instance[count.index].display_name
  value = oci_core_instance.instance[count.index].public_ip
  type = "A"
  ttl = 300
}
